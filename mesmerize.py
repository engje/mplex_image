# Import libraries
import os
import numpy as np
import pandas as pd
import skimage
from skimage import io, measure, segmentation
from skimage.util import img_as_ubyte,  img_as_float
from mplex_image import features
from deepcell.applications import Mesmer
from deepcell.utils.plot_utils import create_rgb_image
from deepcell.utils.plot_utils import make_outline_overlay

def create_two_channel_overlay(df_img,s_round_dapi,se_cyto,regdir,b_denoise=False):
    s_dapi = df_img[(df_img.rounds==s_round_dapi)& (df_img.marker=='DAPI')].index[0]
    img = io.imread(f'{regdir}/{s_dapi}')
    dapi_scale = skimage.exposure.rescale_intensity(img,in_range=(np.quantile(img,0.03),1.5*np.quantile(img,0.99999)))
    dapi = np.clip((dapi_scale),a_min=0,a_max=65535)
    dapi_gamma = skimage.exposure.adjust_gamma(dapi, gamma=0.8)
    imgs_cyto = []
    for s_cyto in df_img[df_img.marker.isin(se_cyto)].index.tolist():
        img = io.imread(f'{regdir}/{s_cyto}')
        cyto_scale = skimage.exposure.rescale_intensity(img,in_range=(np.quantile(img,0.03),1.5*np.quantile(img,0.99999)))
        imgs_cyto.append(cyto_scale)
    mip = np.stack(imgs_cyto).max(axis=0)
    cyto = np.clip((mip),a_min=0,a_max=65535)
    cyto_gamma = skimage.exposure.adjust_gamma(cyto, gamma=0.7)
    #de-noise  
    if b_denoise:
        tv = skimage.restoration.denoise_tv_chambolle(dapi_gamma, weight=0.1)
        cyto_tv = skimage.restoration.denoise_tv_chambolle(cyto_gamma, weight=0.05)
        zdh = np.dstack((tv,cyto_tv,np.zeros(tv.shape)))
    zdh = np.dstack((img_as_ubyte(dapi_gamma),img_as_ubyte(cyto_gamma),np.zeros(dapi_gamma.shape,dtype='uint8')))
    return(zdh)

def find_unsegmented(segdir_in,segdir_out):
    '''
    returns:
    ls_file = list of overlay images that need segmenting
    df_merge = dataframe with matched overlay images and segmentation images
    '''
    df_file = pd.DataFrame(data=os.listdir(f'{segdir_in}'),columns=['overlay_img'])
    df_file['scene'] = [item.split('_')[2] + '_' + item.split('_')[3] for item in df_file.overlay_img]
    df_seg = pd.DataFrame(data = os.listdir(f'{segdir_out}'),columns=['seg_image'])
    df_seg['scene'] = [item.split('_')[0] + '_' + item.split('_')[1] for item in df_seg.seg_image]
    df_merge = df_file.merge(df_seg,on='scene',how='left')
    ls_file = df_merge.loc[(df_merge.seg_image.isna()),'overlay_img'].tolist()
    return(df_merge, ls_file)

def segment_two_channel(app,segdir_in,segdir_out,s_file,image_mpp):
    '''
    run mesmer segmentation on a two channel image
    red=nuclear and green=cytoplasm
    returns:
    saves the labeled tiff of nuclear and cell segmentation
    '''
    img = io.imread(f'{segdir_in}/{s_file}')
    s_scene = s_file.split('_')[2] + '_' + s_file.split('_')[3]
    a_test = np.expand_dims(img[:,:,0:2], axis=0)
    rgb_test = create_rgb_image(a_test, channel_colors=['blue', 'green'])
    #nuc
    test_predictions = app.predict(a_test, image_mpp=image_mpp, compartment='nuclear')
    labels = np.reshape(test_predictions[0],test_predictions[0].shape[0:2])
    io.imsave(f'{segdir_out}/{s_scene}_nuclei{image_mpp}_NucleiSegmentationBasins.tif',labels,check_contrast=False)
    #cell
    try: 
        cell_predictions = app.predict(a_test, image_mpp=image_mpp)
        cell_labels = np.reshape(cell_predictions[0],cell_predictions[0].shape[0:2])
    except: #not working with error
        print('cell seg failed')
        cell_labels = np.zeros(labels.shape,dtype=labels.dtype)
    #discard cells that don't have a matching nucleus
    df = pd.DataFrame(measure.regionprops_table(label_image=cell_labels,intensity_image=labels,properties=('label','area','mean_intensity')))
    ls_drop = df[df.mean_intensity==0].label
    cell_labels[np.isin(cell_labels, ls_drop)] = 0
    io.imsave(f'{segdir_out}/{s_scene}_CytoMarkers_cell{image_mpp}_CellSegmentationBasins.tif',cell_labels,check_contrast=False)
    print(f'done with {s_scene}') 
    #return(cell_labels)

#helper
def _intensity_image_quantile(df_prop,idx,i_quant=.75):
    '''
    helper function to extract the quantile of intensity for each cell membrane
    '''
    label_id = df_prop.loc[idx,'label']
    intensity_image_small = df_prop.loc[idx,'intensity_image']
    image = df_prop.loc[idx,'image']
    pixels = intensity_image_small[image]
    pixels25 = pixels[pixels >= np.quantile(pixels,i_quant)]
    return(pixels25,label_id)

# #features
def extract_mesmer_features(s_scene,df_img,segdir_out,segdir_in,regdir,image_mpp=0.5):
    '''
    Parameters:
    s_scene = unique sample + ROI "scene" name, a.k.a slide_scene
    df_img = dataframe of images with slide_scene column and marker column with biomarker name
    segdir_in = directory with the two-channel overlays
    segdir_out = directory with the nuclear and matched cell segmentation
    regdir = directory with the registered images from which to extract features
    image_mpp = mesmer scaling factor; 20x = 0.5
    Returns:
    dataframe with single cell mean intensity values for nuclear, cytoplasm and cell. 75th percentil of intensity for mrmbrane. also nuclear area, nuclear eccentricity and nuclear centroids. Centroid-0 is the image Y-axis and centroid-1 is the X-axis.
    '''
    labels = io.imread(f'{segdir_out}/{s_scene}_nuclei{image_mpp}_NucleiSegmentationBasins.tif')
    try:
        cell_labels = io.imread(f'{segdir_out}/{s_scene}_CytoMarkers_matchedcell{image_mpp}_CellSegmentationBasins.tif')
        print(f'extracting features {s_scene}')
    except:
        print(f'missing matched {s_scene}')
        return(pd.DataFrame())
    s_dapi = f'Registered-R1_DNA1.Cyto_{s_scene}_c1c2.png'
    dapi = io.imread(f'{segdir_in}/{s_dapi}')[:,:,0]
    df_feat = features.extract_feat(labels,dapi, properties=(['label','area','eccentricity','centroid']))
    df_feat.columns = [f'{item}_nuclei' for item in df_feat.columns]
    df_feat = features.index_format(df_feat,s_scene,'label_nuclei')   
    cyto = features.label_difference(labels,cell_labels)
    membrane, __, __ = features.straddle_label(cell_labels,distance=1) #better
    # tested, worse membrane = contract_label(cell_labels,distance=2)
    d_loc = {'nuclei':labels,'cell':cell_labels,'cytoplasm':cyto,'memp25':membrane}
    #markers (not duplicated)
    df_scene = df_img[df_img.slide_scene==s_scene]#.sort_values('round_ord')
    #loop through markers
    for s_index in df_scene.index:
        df_marker = pd.DataFrame(index=df_feat.index)
        s_marker = df_scene.loc[s_index,'marker']
        #print(f'extracting features {s_marker}')
        if s_marker == 'DAPI':
            s_marker = s_marker + f'{df_scene.loc[s_index,"rounds"].split("R")[1]}'
        intensity_image = io.imread(f'{regdir}/{s_index}')
        for s_loc, a_loc in d_loc.items():
            #top 25th quantile
            if s_loc == 'memp25':
                #print(s_loc)
                df_marker_loc = pd.DataFrame(columns = [f'{s_marker}_{s_loc}'])
                df_prop = features.extract_feat(a_loc,intensity_image, properties=(['intensity_image','image','label']))
                for idx in df_prop.index:
                    pixels25, label_id = _intensity_image_quantile(df_prop,idx)
                    df_marker_loc.loc[label_id,f'{s_marker}_{s_loc}'] = pixels25.mean()
                df_marker_loc['label'] = df_marker_loc.index
                df_marker_loc = features.index_format(df_marker_loc,s_scene) 
                df_marker_loc.drop(f'label',axis=1,inplace=True)
            else:
                df_marker_loc = features.extract_feat(a_loc,intensity_image, properties=(['mean_intensity','label']))
                df_marker_loc.columns = [f'{s_marker}_{s_loc}',f'label']
                df_marker_loc = features.index_format(df_marker_loc,s_scene) 
                df_marker_loc.drop(f'label',axis=1,inplace=True)
            df_marker = df_marker.merge(df_marker_loc, left_index=True,right_index=True,how='left',suffixes=('',f'{s_marker}_{s_loc}'))
        #extract nuclear expansions and use to fill nas
        df_na = df_marker[df_marker.isna().any(axis=1)]
        ls_na = [int(item.split('_cell')[1]) for item in df_na.index]
        ring_labels, grown_labels = features.expand_label(labels,distance=3)
        ring_labels[~np.isin(ring_labels,ls_na)] = 0
        grown_labels[~np.isin(grown_labels,ls_na)] = 0
        membrane_grown, __, __ = features.straddle_label(grown_labels,distance=1)
        df_marker_ring = features.extract_feat(ring_labels,intensity_image, properties=(['mean_intensity','label']))
        df_marker_grown = features.extract_feat(grown_labels,intensity_image, properties=(['mean_intensity','label']))
        df_marker_grown_membrane = features.extract_feat(membrane_grown,intensity_image, properties=(['intensity_image','image','label']))
        df_feat_na = df_marker_ring.merge(df_marker_grown,on='label',suffixes=('_ring','_grown'))
        #add membrane
        for idx in df_marker_grown_membrane.index:
            pixels25, label_id = _intensity_image_quantile(df_marker_grown_membrane,idx)
            df_feat_na.loc[df_feat_na.label==label_id,f'quantile_intensity_mem'] = pixels25.mean()
        df_feat_na = features.index_format(df_feat_na,s_scene)
        #fill in nas
        df_marker.loc[df_feat_na.index,f'{s_marker}_cytoplasm'] = df_feat_na.loc[:,f'mean_intensity_ring']
        df_marker.loc[df_feat_na.index,f'{s_marker}_cell'] = df_feat_na.loc[:,f'mean_intensity_grown'] 
        df_marker.loc[df_feat_na.index,f'{s_marker}_memp25'] = df_feat_na.loc[:,f'quantile_intensity_mem'] 
        #add markers to all featurs
        df_feat = df_feat.merge(df_marker, left_index=True,right_index=True,how='left',suffixes=('',f'{s_marker}'))
    return(df_feat,df_marker_grown_membrane,df_feat_na)    